@B-74932
Feature: Test UNO API Post Functionality to add correspondence from AEX

  Scenario Outline: Test UNO API ability to add correspondence from AEX

    Given I use the correct authorization header
    And I send a recipient email <recipient_email>
    And I send the date <created_date>
    And I add correspondence <note>
    And I send a sender email <sender_email>
    And I use the following <url>
    When I want to send it all as a post
    Then I want to make sure the response_code is the <correct_response_code>

    Examples:
      |recipient_email       | created_date              |note                              |sender_email            |url | correct_response_code|
      |100perocks@gmail.com  |'2017-11-16T14:25:59.852Z' |acceptance code 123               |eddie.merchant@sage.com |3   |200                   |
      |100perocks@gmail.com  |'2017-11-17T10:20:00.111Z' |approval code 0100-213            |eddie.merchant@sage.com |3   |200                   |
      |100perocks@gmail.com  |'2017-11-10:20:00.111Z'    |short date code 888               |eddie.merchant@sage.com |3   |200                   |
      |100perocks@gmail.com  |'2017-11-100000:20:00.111Z'|long date code 333                |eddie.merchant@sage.com |3   |200                   |
