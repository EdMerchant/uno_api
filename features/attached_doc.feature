@B-74450
Feature: Test UNO API Post Functionality to consume signed attached documents from AEX

  Scenario Outline: Test UNO API ability to consume signed attached documents from AEX

    Given I use the correct authorization header
    And I send a file <data>
    And I send the file <type>
    And I use the following <url>
    When I want to send it all as a post
    Then I want to make sure the response_code is the <correct_response_code>

    Examples:
    |data| type|url | correct_response_code|
    |@txt|txt  |1   |204                   |
    |@rtf|rtf  |1   |204                   |
    |@ppt|ppt  |1   |204                   |
    |@pdf|pdf  |1   |204                   |
    |@png|png  |1   |204                   |
    |@gif|gif  |1   |204                   |
    |@epg|epg  |1   |204                   |
    # *note: |@epg|epg  |1   |204                   |
    # *note:  Check with development team as the epg test is a blank file with a non valid extension (should this really return a 204?)