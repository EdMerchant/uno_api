
#Url class init's each of the API's under test

class Url


  def initialize(id)
    @id = id
  end

  #This is the methods being used to Post a url
def post_url
  case @id.to_i
    when 1
      'https://qa.uno.eftsecure.net/WebServices/CoreDataService/DigitalSignatures/40/Signed/AttachedDocuments'
    when 2
      'https://qa.uno.eftsecure.net/WebServices/CoreDataService/Merchants/906024/BankInfo'
    when 3
      'https://qa.uno.eftsecure.net/webservices/CoreDataService/DigitalSignatures#!/Notes/Notes_AddCorrespondence'
    when 4
      'https://qa.uno.eftsecure.net/WebServices/CoreDataService/DigitalSignatures/40/Notes/Internal'
    when 5
      'https://qa.uno.eftsecure.net/WebServices/CoreDataService/Merchants/375034/Corporation'
    when 6
      'https://qa.uno.eftsecure.net/WebServices/CoreDataService/DigitalSignatures/214397/Signed/Status?status=SentToMerchant'
    when 7
      'https://qa.uno.eftsecure.net/WebServices/CoreDataService/DigitalSignatures/214397/Signed/Status?status=SigningStarted'
    when 8
      'https://qa.uno.eftsecure.net/WebServices/CoreDataService/DigitalSignatures/214397/Signed/Status?status=SigningCompleted'
    when 9  #Pending request
      'https://qa.uno.eftsecure.net/WebServices/CoreDataService/DigitalSignatures/214397/Signed/Status?status=Pending'
    when 10  #Declined request
      'https://qa.uno.eftsecure.net/WebServices/CoreDataService/DigitalSignatures/214397/Signed/Status?status=Declined'
    when 11  #Approved request
      'https://qa.uno.eftsecure.net/WebServices/CoreDataService/DigitalSignatures/214397/Signed/Status?status=Approved'
    else
      puts 'url issue'
  end
end
end


