require 'json'
require 'pry'
require 'rspec'
require 'rest-client'
# require 'httpclient'
require 'httparty'
require_relative 'base_url.rb'
require_relative 'data.rb'
require 'test/unit/assertions'
include Test::Unit::Assertions


Given(/^I use the correct authorization header$/) do
  @header = {:Authorization => 'Bearer 1e9z0v4adzsjgfapv228iqlto25b6cb834mc3yoe23uu45tl0gew'}
end

And(/^I send a file (.*)$/) do |data|
  @doc = data
end

And(/^I send the file (.*)$/) do |type|
  @type = type
end

And(/^I use the following (.*)$/) do |url|
  u = Url.new(url)
  @url = u.post_url
end

# When(/^I want to send it all as a post$/) do
#   #@json = {:documentType => '0', :}#
#   @body = {:documentType => '0', :document => @doc, :fileType => @type}
#
#   begin
#     response = RestClient.post(@url, @body, @header)
#   rescue => e
#     puts e.http_body
#   end
#   puts response.code
#   @response = response.code
# end

Then(/^I want to make sure the response_code is the (.*)$/) do |correct_response_code|
  assert_equal(@response, correct_response_code.to_i)
end


And(/^I update the name of a (.*)$/) do |bank_name|
  @bank_name = bank_name
end

And(/^I send update to (.*) and (.*)$/) do |credit_acct_num, credit_route_num|
  @credit_acct_num = credit_acct_num
  @credit_route_num = credit_route_num
end

And(/^I send fees to update (.*) and (.*)$/) do |debit_acct_num, debit_route_num|
  @debit_acct_num = debit_acct_num
  @debit_route_num = debit_route_num
end

And(/^I send a recipient email (.*)$/) do |recipient_email|
  @recipient_email = recipient_email
end

And(/^I send the date (.*)$/) do |created_date|
  @created_date = created_date
end

And(/^I add correspondence (.*)$/) do |note|
  @note = note
end

And(/^I send a sender email (.*)$/) do |sender_email|
  @sender_email = sender_email
end

And(/^I update tax info (.*), (.*) and (.*)$/) do |federal_tax_id, tax_filing_name, tax_filing_state|
  @federal_tax_id = federal_tax_id
  @tax_filing_name = tax_filing_name
  @tax_filing_state = tax_filing_state
end

And(/^I send address update to (.*), (.*), (.*), (.*), (.*) and (.*)$/) do |line1, line2, city, state, postal_code, country|
  @line1 = line1
  @line2 = line2
  @city = city
  @state = state
  @postal_code = postal_code
  @country = country
end

And(/^I send phone number update to (.*)$/) do |phone_number|
  @phone_number = phone_number
end

When(/^I want to send patch$/) do
  @body = {:documentType => '0', :document => @doc, :fileType => @type}

  @url = @url.split(/CoreDataService/)
  puts @url[0]
  # puts Url.@id
  # @url1 = (@url[0] + Url.@id)
  # puts @url1
  begin
    response = Request.execute(:method => :patch, :url => @url, :payload => @body, :headers => @headers)
    #response = RestClient::Request.execute(:method => :patch, url: @url, payload: @body, header: @header)
    #response = RestClient.patch(@url, @body, @header)
    #puts @url
    #puts @Body
  rescue => e
    puts e.http_body
  end
  puts response.code
  @response = response.code
end

And(/^I send location address update to (.*), (.*), (.*), (.*), (.*) and (.*)$/) do |loc_line1, loc_line2, loc_city, loc_state, loc_postal_code, loc_country|
  @loc_line1 = loc_line1
  @loc_line2 = loc_line2
  @loc_city = loc_city
  @loc_state = loc_state
  @loc_postal_code = loc_postal_code
  @loc_country = loc_country
end

And(/^I send location phone number update to (.*)$/) do |loc_phone_number|
  @loc_phone_number = loc_phone_number
end

And(/^I send location email update to (.*)$/) do |loc_email|
  @loc_email = loc_email
end

And(/^I send start date to (.*)$/) do |start_date|
  @start_date = start_date
end

And(/^I send location url to (.*)$/) do |loc_url|
  @loc_url = loc_url
end

And(/^I send business description to (.*)$/) do |bus_desc|
  @bus_desc = bus_desc
end

And(/^I send business monthly volume to (.*)$/) do |monthly_vol|
  @monthly_vol = monthly_vol
end

And(/^I send business average ticket to (.*)$/) do |ave_ticket|
  @ave_ticket = ave_ticket
end

And(/^I send business highest ticket to (.*)$/) do |high_ticket|
  @high_ticket = high_ticket
end

And(/^I send business max exposure to (.*)$/) do |max_exposure|
  @max_exposure = max_exposure
end

And(/^I send owner1 update to (.*), (.*), (.*), (.*)$/) do |first_name1, last_name1, dob1, ssn1|
  @first_name1 = first_name1
  @last_name1 = last_name1
  @dob1 = dob1
  @ssn1 = ssn1
end

And(/^I send owner1 address update to (.*), (.*), (.*), (.*), (.*) and (.*)$/) do |res_line1, res_line2, res_city1, res_state1, res_postal_code1, res_country1|
  @res_line1 = res_line1
  @res_line2 = res_line2
  @res_city1 = res_city1
  @res_state1 = res_state1
  @res_postal_code1 = res_postal_code1
  @res_country1 = res_country1
end

And(/^I send owner1 phone number update to (.*)$/) do |res_phone_number1|
  @res_phone_number1 = res_phone_number1
end

And(/^I send owner1 equity share (.*)$/) do |equity_share1|
  @equity_share1 = equity_share1
end

And(/^I send owner2 update to (.*), (.*), (.*), (.*)$/) do |first_name2, last_name2, dob2, ssn2|
  @first_name2 = first_name2
  @last_name2 = last_name2
  @dob2 = dob2
  @ssn2 =ssn2
end

And(/^I send owner2 address update to (.*), (.*), (.*), (.*), (.*) and (.*)$/) do |res_line1, res_line22, res_city2, res_state2, res_postal_code2, res_country2|
  @res_line1 = res_line1
  @res_line22 = res_line22
  @res_city2 = res_city2
  @res_state2 = res_state2
  @res_postal_code2 = res_postal_code2
  @res_country2 = res_country2
end

And(/^I send owner2 phone number update to (.*)$/) do |res_phone_number2|
  @res_phone_number2 = res_phone_number2
end

And(/^I send owner2 equity share (.*)$/) do |equity_share2|
  @equity_share2 = equity_share2
end


When(/^I want to send it all as a post$/) do
  @body = {:documentType => '0', :document => @doc, :fileType => @type}

  begin
    binding.pry
    response = RestClient.post(@url, @body, @header)
  rescue => e
    puts e.http_body
  end
  puts response.code
  @response = response.code
end