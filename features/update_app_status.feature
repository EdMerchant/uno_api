@B-75337
Feature: Test UNO API Post for Update Application Status Functionality

  Scenario Outline: Test UNO API ability to Update Application Status

    Given I use the correct authorization header
    And I update tax info <federal_tax_id>, <tax_filing_name> and <tax_filing_state>
    And I send address update to <line1>, <line2>, <city>, <state>, <postal_code> and <country>
    And I send phone number update to <phone_number>
    And I send location address update to <loc_line1>, <loc_line2>, <loc_city>, <loc_state>, <loc_postal_code> and <loc_country>
    And I send location phone number update to <loc_phone_number>
    And I send location email update to <loc_email>
    And I send start date to <start_date>
    And I send location url to <loc_url>
    And I send business description to <bus_desc>
    And I send business monthly volume to <monthly_vol>
    And I send business average ticket to <ave_ticket>
    And I send business highest ticket to <high_ticket>
    And I send business max exposure to <max_exposure>
    And I send owner1 update to <first_name1>, <last_name1>, <dob1>, <ssn1>
    And I send owner1 address update to <res_line1>, <res_line2>, <res_city1>, <res_state1>, <res_postal_code1> and <res_country1>
    And I send owner1 phone number update to <res_phone_number1>
    And I send owner1 equity share <equity_share1>
    And I send owner2 update to <first_name2>, <last_name2>, <dob2>, <ssn2>
    And I send owner2 address update to <res_line1>, <res_line22>, <res_city2>, <res_state2>, <res_postal_code2> and <res_country2>
    And I send owner2 phone number update to <res_phone_number2>
    And I send owner2 equity share <equity_share2>
    And I update the name of a <bank_name>
    And I send update to <credit_acct_num> and <credit_route_num>
    And I send fees to update <debit_acct_num> and <debit_route_num>
    And I use the following <url>
    When I want to send it all as a post
#    Then I want to make sure the response_code is the <correct_response_code>

    Examples:
      |federal_tax_id  | tax_filing_name    |tax_filing_state |line1            |line2 |city      |state   |postal_code|country |phone_number|loc_line1     |loc_line2 |loc_city |loc_state |loc_postal_code |loc_country |loc_phone_number |loc_email          |start_date                 |loc_url                |bus_desc   |monthly_vol |ave_ticket |high_ticket |max_exposure |first_name1 |last_name1 |dob1                      |ssn1        |res_line1     |res_line2 |res_city1 |res_state1 |res_postal_code1 |res_country1 |res_phone_number1 |equity_share1 |first_name2 |last_name2 |dob2                     |ssn2        |res_line1      |res_line22 |res_city2 |res_state2 |res_postal_code2 |res_country2 |res_phone_number2 |equity_share2 |bank_name      |credit_acct_num |credit_route_num |debit_acct_num |debit_route_num|url| correct_response_code|
      |12-084512       |eSmith DBA Demose   |Georgia          |265 Sharon Dr    |Ste B |Covington |Georgia |30016      |USA     |2144090172  |265 Hoglen Dr |Basement  |Covington|GA        |30016           |USA         |6785551212       |blindspot@sage.com |2017-11-28T15:35:49.916Z   |https://www.blind.com  |B- Credit  |1500        |15         |50          |100          |James       |Paterson   |2017-11-28T15:35:49.923Z  |123-45-6789 |123 Cutty Dr  |Ste 2-A   |Covington |GA         |30014            |USA          |2147903698        |50            |Indieo      |Maze       |2017-11-28T15:35:49.938Z |987-65-4321 |323 Safeway Dr |Route 81   |Covington |GA         |30015            |USA          |4097709212        |50            |Georiga's Own  | 298753159      |198575325        |987654321      |654215875      | 9 |200                   |