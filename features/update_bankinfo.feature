@B-74755
Feature: Test UNO API Patch Functionality to updated Bank Info

  Scenario Outline: Test UNO API ability to updated Bank Info

    Given I use the correct authorization header
    And I update the name of a <bank_name>
    And I send update to <credit_acct_num> and <credit_route_num>
    And I send fees to update <debit_acct_num> and <debit_route_num>
    And I use the following <url>
    When I want to send patch
    Then I want to make sure the response_code is the <correct_response_code>

    Examples:
      |bank_name        | credit_acct_num           |credit_route_num |debit_acct_num             |debit_route_num |url| correct_response_code|
      |'Bank of Smith'  |1450018327                 |767008777        |7740018477                 |767008777       |2  |204                   |
#      |'Bank of Frank'  |2170018327                 |3512087770       |7740018477                 |351208777       |2  |204                   |
#      |'Smith Bank'     |1450018327                 |555213214        |7740018477012345678902333  |767008777       |2  |204                   |
#      |'Covington Bank' |1450018327                 |767008777        |77400184770123456789011111 |767008777       |2  |204                   |
#      |'Georgia Bank'   |5774001847754131833321423  |7670087771       |7740018477                 |3512073870      |2  |204                   |
#      |'Winkles Bank'   |17941577400184775413183336 |767008777        |7740018477                 |767008777       |2  |204                   |
#      |'Johnson Bank'   |55413                      |767008777        |7740018477                 |767008777       |2  |204                   |
#      |'Mercela Bank'   |5541318333                 |767008           |7740018477                 |767008777       |2  |204                   |