
Feature: Test UNO API Patch Functionality

  Scenario Outline: Test UNO API ability to update Corporation Info

    Given I use the correct authorization header
     And I update tax info <federal_tax_id>, <tax_filing_name> and <tax_filing_state>
     And I send address update to <line1>, <line2>, <city>, <state>, <postal_code> and <country>
     And I send phone number update to <phone_number>
     And I use the following <url>
     When I want to send patch
  #   Then I want to make sure the response_code is the <correct_response_code>

    Examples:
      |federal_tax_id  | tax_filing_name    |tax_filing_state |line1            |line2 |city      |state   |postal_code|country |phone_number|url| correct_response_code|
      |123456789       |eSmith DBA Demose   |Georgia          |265 Sharon Dr    |Ste B |Covington |Georgia |30016      |USA     |7137701212  |5  |204                   |